const mongoose = require('mongoose');

const RecipeSchema = new mongoose.Schema({
    recipe: String,
    img: {
        type: [String],
        default: []
    },
    price: Number,

})

const Recipe = new mongoose.model("Recipe", RecipeSchema)

module.exports = {
    Recipe
}
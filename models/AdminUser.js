const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    email: {
        type:String,
        required:true,
        index: {
            unique:true
        }
    },
    password: {
        type:String,
    },
    firstname: {
        type:String,
        required:true
    },
    lastname:{
        type:String,
        required:true
    },
    contactNumber: {
        type:String,
        required:true
    },
    address:{
        type:[String],
        default:[""]
    }
})

const User = mongoose.model('User',userSchema);

module.exports = {
    User
}
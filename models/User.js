
const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    password: {
        type: String,
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    contactNumber: {
        type: String,
    },
    address: {
        type: [String],
        default: [""]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Order"
    }

}, {
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret.password;
            delete ret.__v;
            delete ret._id;

        }
    }
})

const User = mongoose.model('User', userSchema);

module.exports = {
    User
}

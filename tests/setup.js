const { MongoMemoryServer } = require("mongodb-memory-server");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");


let mongo;

beforeAll(async () => {
    mongo = await MongoMemoryServer.create();
    // return mongo as a promise or check the docs
    const mongoURI = await mongo.getUri();

    await mongoose.connect(
        mongoURI, { useNewUrlParser: true, useUnifiedTopology: true }
    )
})


beforeEach(async () => {
    const collections = await mongoose.connection.db.collections();

    for (const collection of collections) {
        await collection.deleteMany({});
    }
})


afterAll(async () => {
    await mongo.stop();
    await mongoose.connection.close();
})


global.signin = () => {
    const payload = {
        id: new mongoose.Types.ObjectId().toHexString()
    }

    const token = jwt.sign(payload, process.env.JWT_KEY);


    const header = {
        "x-access-token": token
    }

    return header;
}
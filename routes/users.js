const userRouter = require("express").Router()
const { middlewares } = require("@azaxxc/common/src");
const { getAllUsers, getUserById, addUser, deleteUser, updateUser, updateUserPreferences } = require("../controllers/User").userOps

// CRUD
userRouter.get("/", middlewares.authAPIs, async (req, res, next) => {
    try {
        const users = await getAllUsers(req);
        res.send(users);
    } catch (err) {
        next(err);
    }
});
userRouter.get("/:id", middlewares.authAPIs, async (req, res, next) => {
    try {
        const user = await getUserById(req);
        res.send(user);
    } catch (err) {
        next(err);
    }
})
userRouter.post("/add", async (req, res, next) => {
    try {
        const user = await addUser(req);
        res.send(user);
    } catch (err) {
        next(err);
    }
});
userRouter.delete("/delete/:id", middlewares.authAPIs, async (req, res, next) => {
    try {
        const user = await deleteUser(req);
        res.send(user);
    } catch (err) {
        next(err);
    }
})
userRouter.put("/update/:id", middlewares.authAPIs, async (req, res, next) => {
    try {
        const user = await updateUser(req);
        res.send(user);
    } catch (err) {
        console.log(err);
        next(err);
    }
})

userRouter.put("/update/:id/preferences", middlewares.authAPIs, async (req, res, next) => {
    try {
        const user = await updateUserPreferences(req);
        res.send(user).status(201);
    } catch (err) {
        console.log(err);
        next(err);
    }
})

module.exports = userRouter;

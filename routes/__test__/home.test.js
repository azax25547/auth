const request = require("supertest");
const app = require("../../app");
const { registerUser, login} = require("./misc");

const username = "adam@help.com"
const password = "password"
const firstname = "Aditya"
const lastname = "Mishra"
const contact = "1234567890"



describe("GET /status", () => {
    it('returns a 200 on successful status', async () => {
        return request(app)
            .get('/auth/status')
            .expect(200)
    })
})

describe("POST /register", () => {
    it('returns a 201 on successful signup', async () => {
        const{body:user} = await request(app)
            .post('/auth/register')
            .send({
                username,
                password,
                firstname,
                lastname,
                contact
            })
            .expect(201)

    })

    it("returns a 400 on a duplicate signup", async () => {

        await registerUser(username,password,firstname,lastname,contact);

        return request(app)
            .post('/auth/register')
            .send({
                username,
                password,
                firstname,
                lastname,
                contact
            })
            .expect(400)
    })
})


describe("POST /login", () => {
    it("should return 200 when successful login", async () => {
        await registerUser(username,password,firstname,lastname,contact);

        return request(app)
            .post('/auth/login')
            .send(
                {
                    username,
                    password
                }
            )
            .expect(200)

    })

    it("should return 401 for incorrect Email or Password", async () => {
        await registerUser(username,password,firstname,lastname,contact);

        return request(app)
            .post('/auth/login')
            .send(
                {
                    username,
                    password: "aweasd"
                }
            )
            .expect(401)
            .expect({
                "message": "Either Provided Username or password is incorrect. Please Check the credentials."
            })
    })
})



describe("GET /", () => {
    it("Should return 200 for successful Logged in user", async () => {
        // Register the User
        await registerUser(username,password,firstname,lastname,contact);
        // login the user
        const re = await login(username, password);

        return request(app)
            .get("/auth/")
            .set('x-access-token', re.body.user.token)
            .expect(200)
    })

    it("should get empty user details for Non-Authenticated Users", async () => {
        return request(app)
            .get("/auth/")
            .expect(403)
            .expect({ errors: { message: 'Failed to authenticate token.', data: '' } })
    })
})

describe("GET /logout", () => {
    it("should logout the authenticated user successfully", async () => {
        // Register the User
        await registerUser(username,password,firstname,lastname,contact);
        // login the user
        await login(username, password);
        return request(app)
            .get("/auth/logout")
            .expect({})
            .expect(200)
    })
})

const app = require("../../app");
const request = require("supertest");

module.exports = {
    login: async function(username, password) {
        return request(app)
            .post('/auth/login')
            .send({
                username,
                password
            })
},
    registerUser: async function(username, password,firstname,lastname,contact) {
        await request(app)
            .post('/auth/register')
            .send({
                username,
                password,
                firstname,
                lastname,
                contact
            })
    }
}
const homeRouter = require("express").Router()

const { middlewares } = require("@azaxxc/common/src");
const { login, logout } = require("../controllers/User").authOps;
const { addUser } = require("../controllers/User").userOps;


homeRouter.get("/status", (req, res) => {
    res.status(200).json({
        "message": "Hello There!"
    });
})

homeRouter.post("/login", async (req, res, next) => {
    try {
        await login(req, res, next);
    } catch (err) {
        next(err)
    }
})


homeRouter.post("/register", async (req, res, next) => {
    try {
        const user = await addUser(req);
        res.status(201).send(user);
    } catch (err) {
        next(err);
    }
})


homeRouter.get("/", middlewares.authAPIs, (req, res) => {
    res.send({
        currentUser: req.user
    })
})

homeRouter.get('/logout', logout);

module.exports = homeRouter;

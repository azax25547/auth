const { User } = require("../models/User");
const passport = require('passport');
const bcrypt = require('bcrypt');
const { errors } = require("@azaxxc/common/src");
require("dotenv").config();



const userOps = {
    async getAllUsers(req) {
        let users;
        try {
            users = await User.find({}).populate("order");
        } catch {
            throw new errors.HTTPError("No users found")
        }
        return users;

    },
    async getUserById(req) {
        let user;
        try {
            user = await User.find({ _id: req.params.id }).populate("order");


        } catch (err) {
            console.log(err);
            throw new errors.HTTPError("No User found")
        }
        return user || {};

    },
    async addUser(req) {
        const { firstname, lastname, username, password, contact } = req.body;
        let user;
        try {
            //first search if user already exists or not
            user = await User.find({
                username: username.toString()
            })
            if (user.length !== 0) {
                errors.HTTPError({ message: "User Exists Already" }, "", 400)
                return false
            }

            //hash the password
            const hashedPassword = await bcrypt.hash(password, 10);


            user = await User.create({
                firstname: firstname.toString(),
                lastname: lastname.toString(),
                contactNumber: contact.toString(),
                email: username.toString(),
                password: hashedPassword.toString()
            })

            await user.save();

        } catch (err) {
            throw new errors.InternalServerError(400);
        }
        return user;
    },
    async updateUser(req) {
        let { ...details } = req.body;
        const { id } = req.params;
        let user;
        try {
            user = await User.findByIdAndUpdate(id, {
                ...details
            }, {
                useFindAndModify: false,
                new: true
            });
        } catch (err) {
            throw new errors.HTTPError("Unable to update the user")
        }
        return user;

    },
    async deleteUser(req) {
        const { id } = req.params;
        try {
            await User.findByIdAndDelete(id);
        } catch (e) {
            throw new errors.HTTPError("Unable to delete the user")
        }
        return [];
    },
    async updateUserPreferences(req) {
        let { ...details } = req.body;
        let { id } = req.params;
        let user;

        try {
            user = await User.findById(id);
            await user.save()


        } catch (err) {
            throw new errors.HTTPError("Unable to update Preference", err.message, 400);
        }

        return user;
    }
}

const authOps = {
    async login(req, res, next) {
        passport.authenticate('local', (err, user) => {
            if (err) {
                console.error({
                    message: "Passport Authentication Error",
                    data: err
                })
            }
            if (!user) {
                return res.status(401).send({
                    message: "Either Provided Username or password is incorrect. Please Check the credentials."
                })
            }
            req.logIn(user, (lgError) => {
                if (lgError) {
                    console.error({
                        message: "Login Error",
                        data: lgError
                    })
                }
                req.session.jwt = user?.token;
                return res.status(200).send({
                    user
                });
            })
        })(req, res, next)
    },

    logout(req, res) {
        req.session.jwt = null;
        req.logout();
        res.send({})
    }
}
module.exports = {
    userOps,
    authOps
}

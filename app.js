const express = require('express')
const { json } = require('body-parser');
const passport = require("passport")
const session = require("express-session")
const helmet = require("helmet");
const cors = require("cors");

require("dotenv").config();
require("./config/passport-setup");

const userRouter = require("./routes/users");
const homeRouter = require("./routes/home");
const { middlewares } = require('@azaxxc/common/src');

const app = express()

app.set('trust proxy', true);

app.use(helmet())

app.use(json())
app.use(cors(
    {
        origin: "http://localhost:3000", // allow to server to accept request from different origin
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        credentials: true, // allow session cookie from browser to pass through
    }
))

app.use(session({
    cookie: { maxAge: 60000000 },
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}))

app.use(passport.initialize())
app.use(passport.session());

app.use("/auth", homeRouter);
app.use("/user", userRouter);

app.use(middlewares.errorhandler);


module.exports = app;

const { CustomError } = require("./CustomError");

class DatabaseError extends CustomError {
    constructor(data, message = "Bad Request") {
        super({
            message,
            data,
            statusCode: 501,
            errorName: "DatabaseError"
        })
    }
}


module.exports = {
    DatabaseError
}
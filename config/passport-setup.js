const passport = require("passport")
const LocalStrategy = require("passport-local")
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken")
const { User } = require("../models/User");

function authUser(user) {
    return jwt.sign({ id: user._id }, process.env.JWT_KEY, {
        expiresIn: 86400 // expires in 24 hours
    });
}

passport.use(new LocalStrategy(async (username, password, done) => {
    let user;
    try {
        user = await User.findOne({
            email: username,
        })

        if (!user) {
            return done(null, false);
        }

        bcrypt.compare(password, user.password)
            .then(match => {
                if (match) {
                    const token = authUser(user)
                    return done(null, {
                        _id: user.id,
                        firstname: user.firstname,
                        lastname: user.lastname,
                        email: user.email,
                        isAdmin: user.isAdmin,
                        token
                    });
                }
                return done(null, false);
            })
            .catch(err => {
                return done(err, false)
            })

    } catch (err) {
        console.log("Error found", err);
    }

}))



passport.deserializeUser(async function (id, done) {
    const user = await User.findById(id);
    done(null, user)
});

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

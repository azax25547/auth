const jwt = require("jsonwebtoken");
const { HTTPError } = require("../utils/HTTPError");

const authAPIs = (req, res, next) => {
    let token = req.session.jwt;
    if (!token) {
        throw new HTTPError('No token provided.', "", 403);
    }
    jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {
        if (err) throw new HTTPError('Failed to authenticate token.', '', 403);

        return next();
    })
}

module.exports = { authAPIs };
const mongoose = require("mongoose");
const app = require("./app");

const start = async () => {
    try {
        await mongoose.connect(`mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_USERNAME_PASSWORD}@${process.env.MONGO_DB_URI}`, {
            useCreateIndex: true,
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
    } catch (err) {
        console.log(err);
    }

    app.listen(4000, () => {
        console.log("Server running on 4000!")
    })

}

start();
